package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the JogAmp JOCL library.
 * 
 * @author Stephane Dallongeville
 */
public class JOCLPlugin extends Plugin implements PluginLibrary
{
    static
    {
        try
        {
            // load native library
            Plugin.loadLibrary(JOCLPlugin.class, "jocl");
        }
        catch (Throwable t)
        {
            System.err.println("Could not load jocl native library.");
            System.err.println(t.getMessage());
        }
    }
}
